+++
title = '{{ replace .File.ContentBaseName "-" " " | title }}'
description = ""
externalUrl = ""
date = {{ .Date }}
lastmod = ""
draft = true
tags = []
series = []
authors = []
showAuthor = true
showAuthorsBadges = true
showDate = true
showDateUpdated = false
showReadingTime = true
showTaxonomies = true
showTableOfContents = true
showWordCount = true
+++
