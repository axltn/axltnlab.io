+++
title = "第7回「サイクロトロン」"
date = "2023-02-13"
weight = 30
+++

{{< youtube id="degTzpaiJeI" >}}

<!--more-->

- YouTube: https://youtu.be/degTzpaiJeI
- [講義スライド](https://drive.google.com/file/d/1q6fHkEuZDiLz0TEVJR3i93l2lqD5hDup/view?usp=share_link)
