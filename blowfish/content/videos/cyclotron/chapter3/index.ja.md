+++
title = "第3回「運動方程式」"
date = "2023-02-13"
weight = 70
+++

{{< youtube id="GfwACcZKPHM" >}}

<!--more-->

- YouTube: https://youtu.be/GfwACcZKPHM
- [講義スライド](https://drive.google.com/file/d/1Dzm9tlilsFYH_jApcihXvql4utQ0cYyu/view?usp=share_link)
