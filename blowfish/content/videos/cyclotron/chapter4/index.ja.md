+++
title = "第4回「等加速度直線運動」"
date = "2023-02-13"
weight = 60
+++


{{< youtube id="ByFiSYAJYwA" >}}

<!--more-->

- YouTube: https://youtu.be/ByFiSYAJYwA
- [講義スライド](https://drive.google.com/file/d/1WB6nPTu58xMFYFMYGoaKgwemq1mvn3j2/view?usp=share_link)
