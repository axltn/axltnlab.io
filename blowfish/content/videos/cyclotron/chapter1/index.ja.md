+++
title = "第1回「等加速度直線運動」"
date = "2023-02-13"
weight = 90
+++

{{< youtube id="dE4TYjlpMcA" >}}

<!--more-->

- YouTube: https://youtu.be/dE4TYjlpMcA
- [講義スライド](https://drive.google.com/file/d/1T5565la-HiE4q1fAr_XSUSl8bOpBEaBM/view?usp=share_link)
