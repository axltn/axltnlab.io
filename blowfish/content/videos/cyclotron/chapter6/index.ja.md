+++
title = "第6回「円運動」"
date = "2023-02-13"
weight = 40
+++

{{< youtube id="EeFH8QpDhsc" >}}

<!--more-->

- YouTube: https://youtu.be/EeFH8QpDhsc
- [講義スライド](https://drive.google.com/file/d/1DaAznx0f3TlJ6pXnR4f6ZhMRCIvf2ovx/view?usp=share_link)
