+++
title = "AxeLatoon"
+++

## 学校で加速器を作っちゃおう！

加速器を使った最先端研究を行うKEKや理研のメンバーと総研大の大学院生を中心に、学校教育と相補的な実際の物づくりによる教育の土壌を作ることを目指しています。


{{< button href="booklet.pdf" target="_self" >}}
パンフレットを読む
{{< /button >}}

それぞれの学校での活動の様子も公開しています。

{{< button href="posts/" target="_self" >}}
研究ログを読む
{{< /button >}}
