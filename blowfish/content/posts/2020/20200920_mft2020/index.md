+++
title = "Maker Faire Tokyo 2020 にブース出展"
linkTitle = "Maker Faire Tokyo 2020 にブース出展"
date = "2020-09-20"
aliases = ["/blog/2020/09/20"]
tags = ["Maker Faire Tokyo"]
+++

2020年10月3日（土）・4日（日）に東京ビッグサイトで開催される **Maker Faire Tokyo** にブース出展します。
``D / 02-01``、``D / 02-02`` で展示をしています。
来場を予定されている方はぜひ遊びに来てください。

<!--more-->

## 展示物

- ``D / 02-01`` [AxeLatoon：学校で加速器を作っちゃおう](https://makezine.jp/event/makers-mft2020/m0057/)
- ``D / 02-02`` [気軽に作れる自宅粒子加速器](https://makezine.jp/event/makers-mft2020/m0122/)

## 配布パンフレット

- [AxeLatoon : 学校で加速器を作っちゃおう（PDF）](https://drive.google.com/open?id=1ik_k2XniHcy6cuiCPrvAaOQIjMm0XNO_)
