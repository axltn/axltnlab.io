+++
title = "小山高専で大阪大学の守實さんが次世代がん治療に関して紹介する説明会を行いました"
linkTitle = "小山高専で大阪大学の守實さんが説明会を開催"
date = "2022-03-11"
authors = ["masashio"]
tags = ["説明会", "小山高専"]
aliases = ["/blog/2022/03/11/"]
+++

大阪大学の守實さんに「次世代のがん治療のための工学的アプローチ」というテーマで研究紹介してもらいました。
近年開発が盛んな加速器BNCTシステムによる実際のがん治療のための研究開発ということで、
がん治療効果の評価などに関して活発な質問意見が交わされました。

説明会のあと、明日の物理学会Jr.セッション発表にむけた
資料の最終確認などを行いました。

![がん治療効果の評価に関してするどい質問が飛び交いました](DSCN3028_featured.JPG "がん治療効果の評価に関してするどい質問が飛び交いました")

![明日の物理学会Jr.セッションでの発表にむけて最終チェックを行いました](DSCN3030.JPG "明日の物理学会Jr.セッションでの発表にむけて最終チェックを行いました")
