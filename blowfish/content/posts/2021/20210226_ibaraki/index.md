+++
title = "サイクロトロン加速器の組み立て開始"
linkTitle = "サイクロトロン加速器の組み立て開始"
date = "2021-02-26"
authors = ["hattori"]
tags = ["茨城高専", "活動"]
aliases = ["/blog/2021/02/26"]
+++

真空チェンバーとD電極の加工が完了し、本日2月26日（金）から組み立て作業を開始しました。
チューナー、電源、真空ポンプなどが机の上から床にまで広がっています。
フィラメント部分のはんだ付けが私の宿題となりました。

<!--more-->

![真空チェンバーにD電極が入り、サイクロトロン加速器らしくなってきました！](IMG_0286_featured.jpg "真空チェンバーにD電極が入り、サイクロトロン加速器らしくなってきました！")
