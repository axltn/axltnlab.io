+++
title = "小山高専で理研の高梨さんによる説明会を行いました"
linkTitle = "小山高専で理研の高梨さんによる説明会を開催"
date = "2021-11-10"
authors = ["masashio"]
tags = ["説明会", "小山高専"]
aliases = ["/blog/2021/11/10/"]
+++

理研の高梨さんに**自宅サイクロトロン加速器**について紹介してもらいました。

1年生の授業ではまだ電磁気学を習っていませんが、Dee電極の構造やビーム粒子の運動に関して
積極的に質問が飛び交いました。
講演の後は電磁石の磁場測定や真空ポンプの動作確認などの作業を一緒に行いました。

![理研・高梨さんの話のあと、サイクロトロン加速器に関してたくさんの質問が飛び交いました](IMG_20211110_170321_featured.jpg "理研・高梨さんの話のあと、サイクロトロン加速器に関してたくさんの質問が飛び交いました")
