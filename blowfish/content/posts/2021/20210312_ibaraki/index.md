+++
title = "銅ガスケットの取り付け"
linkTitle = "銅ガスケットの取り付け"
date = "2021-03-12"
authors = ["masashio"]
aliases = ["/blog/2021/03/12"]
tags = ["茨城高専", "活動"]
draft = true
+++

![ターボ分子ポンプ（TMP）に銅ガスケットをつけているところ](featured_IMG_0348.jpg "ターボ分子ポンプ（TMP）に銅ガスケットをつけているところ")
