+++
title = "津山高専で加速器運転に向けた真空引き試験を行いました"
linkTitle = "津山高専で加速器製作活動を実施"
date = "2024-01-17"
authors = ["ct-tsuyama", "masashio"]
tags = ["津山高専"]
aliases = ["/blog/2024/01/17/"]
+++


2024年1月17日に津山高専で加速器製作活動を行いました。
津山高専では、本科3年生が全系横断演習Ⅰの一環として静電加速器の設計と製作に取り組んでいます。
現在、加速器本体が完成し、真空ポンプへの接続試験を実施しています。

この日の活動では、はじめて稼働させるダイアフラムポンプとターボ分子ポンプの動作試験を行い、
0.001Pa程度までの高真空を達成できることを確認しました。
その後、加速器本体のアクリルパイプに接続し、真空引き試験を実施しました。
リークが発生したため、最終的には1Pa程度までの真空度しか達成できませんでしたが、
リーク箇所の特定に成功しました。
今後は補修を行い、再び真空試験を実施し、フィラメント点火などを進める予定です。

![津山高専の佐々井先生と授業で加速器設計・製作を進める学生さんと真空ポンプの動作確認を行いました](P1173440_featured.JPG "津山高専の佐々井先生と授業で加速器設計・製作を進める学生さんと真空ポンプの動作確認を行いました。")
