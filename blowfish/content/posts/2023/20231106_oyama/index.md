+++
title = "小山高専のチームが国際学会8th STI Gigaku 2023で加速器製作について発表しました"
linkTitle = "小山高専のチームが国際学会でポスター発表"
date = "2023-11-06"
authors = ["ct-oyama", "masashio"]
tags = ["小山高専", "研究発表", "受賞", "活動"]
aliases = ["/blog/2023/11/06/"]
+++

2023年11月6日に、新潟県長岡市で開催された国際学会「8th STI Gigaku 2023」で、
小山高専の加速器製作チーム「アテーナ」を代表し、小暮さんと椎名さんがポスター発表を行いました。

彼らはそれぞれ、「First attempt!? KOSEN students built small accelerators」と「Design and Development of Power Supply for Proton Generation and Biasing in Hand Size Cyclotron Accelerator」という題目で、
現在小山高専で進行中の小型サイクロトロン加速器の製作やビーム観測に向けた初測定、そして陽子発生のためのフィラメント電源の詳細設計について報告しました。

この発表は、高専生が実際に加速器を製作し、その進行状況を学術的に共有するという点で非常に注目されました。
その証として、小暮さんはBest Research Presentation Awardを受賞しました。
発表のコアタイム中は常に聴講者が訪れ、質疑応答も絶え間なく行われたそうです。

二人にとっては、はじめての学会であり、初の英語での発表という新たな挑戦でしたが、非常に貴重な経験を積むことができたと思います。これらの経験が、今後の研究やキャリアにおいて、彼らにとって大きな自信となり、さらなる飛躍への助けとなることと思います。

このように、学生たちが自らの研究成果を世界に向けて発信し、そのプロセスで学び成長することは、
教育の真髄と言えるでしょう。今後も彼らの研究活動を全力でサポートしていきたいと思います。

![小暮さんのポスター発表「First attempt!? KOSEN students built small accelerators」](PXL_20231106_061110567.jpg "小暮さんのポスター発表「First attempt!? KOSEN students built small accelerators」")

![椎名さんのポスター発表「Design and Development of Power Supply for Proton Generation and Biasing in Hand Size Cyclotron Accelerator」](PXL_20231106_061117511.MP.jpg "椎名さんのポスター発表「Design and Development of Power Supply for Proton Generation and Biasing in Hand Size Cyclotron Accelerator」")

![小暮さんがBest Research Presentation Awardを受賞](IMG_20231106_174510_featured.jpg "小暮さんがBest Research Presentation Awardを受賞")

## 関連リンク

- [8th STI Gigaku 2023](https://sites.google.com/view/8th-sti-gigaku-2023/english?authuser=0)
