# AxeLatoon

- GitLab Repos : https://gitlab.com/axltn/axltn.gitlab.io/
- GitLab Pages : https://axltn.gitlab.io/
- KEK Webpages : https://www2.kek.jp/axltn/

---

## ウェブブラウザで編集する

- この GitLab リポジトリをブラウザで開く
- 該当のファイルを開く
  - パスを辿ってファイルを開く
  - `Web IDE` を使って開く
- 編集して保存（コミット）する
  - ウェブ上で編集する場合のプレビュー機能はない
  - `main`ブランチで作業した場合、保存（コミット）するとテストサイトの自動ビルドが実行される

---

## ローカルで編集する

- （初回のみ）この GitLab リポジトリをチェックアウトする（初回のみ）
- リポジトリの状態を確認する（`git status`）
  - 最新版（`HEAD`）になっているか
  - 作業するブランチが正しいか（基本は `master` で OK）
  - 前回の作業からの残り物がないか、など
- 好みのエディタでディレクトリ/ファイルを開く
  - エディタは `Visual Studio Code (VSCode)` をおすすめします
  - もちろん `Emacs` / `Vim` でもよいです
- ライブプレビュー機能
  - ターミナルで `hugo server` をする
  - 編集しながらプレビューを確認することができる
- 編集が終わったらローカルのリポジトリにコミットする（`git commit`）
  - コミット時のメモが統一できるよう `commitizen` を導入している
- GitLab リポジトリにプッシュする（`git push`）
  - `main`ブランチへのプッシュが完了するとテストサイトの自動ビルドが実行される

---

## 編集に必要な記法

### Markdown 記法

- [Markdown - Wikipedia](https://ja.wikipedia.org/wiki/Markdown)は文書をシンプルにマークアップする記法です。
- 記事の本文を記述するために使います。この `README.md` も Markdown 記法で書いてます。
- 詳しくは[日本語 Markdown ユーザー会](https://www.markdown.jp/what-is-markdown/)などを参考にしてください。

### フロントマター

- 記事のメタデータを記述する場所です。静的サイトジェネレーター（SSG）では一般的な概念かもしれません。
- 呼び方や記述方法はSSGやCMSなどによって異なります。`Hugo` では `TOML` / `YAML` / `JSON` 形式で書くことができます。
- 詳しくは[Front Matter - Hugo](https://gohugo.io/content-management/front-matter)を参考にしてください。

### ショートコード

- 記事本文に特定のタグを挿入する記法です。
- 画像の挿入など、Markdown記法ではちょっと弱いところをカバーできます。
- CMSでウィジェットやコンポーネントと呼ばれている機能に近いと思います。
- 詳しくは[Shortcodes - Hugo](https://gohugo.io/content-management/shortcodes/)を参考にしてください。

---

## テストサイトの確認方法

- `main`ブランチにコミット（or プッシュ）があると自動ビルド（GitLab CI）を実行します。
- 実行状況は [パイプライン](https://gitlab.com/axltn/axltn.gitlab.io/-/pipelines) で確認できます。
- 実行内容は [.gitlab-ci.yml](https://gitlab.com/axltn/axltn.gitlab.io/-/blob/master/.gitlab-ci.yml) で設定しています
  - 一番大事な `npm run build` は [package.json](https://gitlab.com/axltn/axltn.gitlab.io/-/blob/master/package.json) で設定しています
  - 下書きもビルドされるようになっている（`hugo -D`）

---

## ローカルで確認する

```console
$ git clone --recursive git@gitlab.com:axltn/axltn.gitlab.io.git
Cloning into 'axltn.gitlab.io'...
...

$ cd blowfish
$ hugo -e blowfish server
...
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
```

---

## ローカルでビルドする

```console
$ cd blowfish
$ hugo -e blowfish
```

---

## `www2`にアップロード

```console
$ bash deploy.sh
```

- ローカルでビルド -> www2 に `rsync` を使って同期する一連の作業を `deploy.sh`にまとめた
  - アップロードできるのは大谷
  - SSH 接続は別に設定しておく（`~/.ssh/config`）

### アップロードするスクリプトでやってること

- アップロードするファイルは毎回作り直す
- ファイル／ディレクトリにグループの編集権限を付与する

```bash
$ rm -rf public/
$ hugo -b https://www2.kek.jp/axltn
$ chmod -R g+rw public/
$ rsync -auvzn --delete public/ kek2:~/axltn/
```

---

## GitLab CI の設定

- `GitLab`上でサイトを自動構築するように設定している
- 以下の 2 ファイルが大事。勝手に触らないようにする

- `gitlab-ci.yml`

---

## テーマの更新

```bash
$ git submodule update --remote
$ git status
$ git commit -m "Updating theme submodules"
$ git push
```
