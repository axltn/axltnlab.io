## 1.0.11 (2024-09-25)

### Fix

- **blowfish/content/docs**: 整理した
- **blowfish/content/docs/overview/index.md**: 概要を設定した

### Refactor

テーマを変更したため、不要になったファイルを削除した

- **assets/**: 削除した
- **blowfish/content/docs**: ファイルを移動した
- **blowfish/content/docs/_index.md**: ショートコードを削除した
- **blowfish/content/docs/_index.md**: 拡張子を変更した
- **blowfish/content/docs/overview**: 整理した
- **blowfish/content/videos/cyclotron**: 資料のURLを整理した
- **blowfish/content/videos/cyclotron/**: スライドを移動した
- **config/**: 削除した
- **content/en/blog/**: 削除した
- **content/en/docs/**: 削除した
- **content/en/posts/**: 削除した
- **content/ja/**: 削除した
- **content/ja/blog/**: 削除した
- **content/ja/community/**: 削除した
- **content/ja/docs/**: 削除した
- **content/ja/docs/**: 削除した
- **content/ja/videos/**: 削除した
- **i18n/ja.toml**: 削除した
- **layouts/404.html**: 削除した
- **package.json**: 削除した
- **static/favicons/**: faviconsを移動した（もう使ってないかも）

## 1.0.10 (2024-09-25)


### Published

- **blowfish/content/posts/2024/20240725_kagawa/index.md**: 公開した

### Added

- **blowfish/content/posts/2024/20240725_kagawa/index.md**: 香川高専の記事を追加した

### Fixed

- **blowfish/content/posts/2024/20240301_kagawa/index.md**: 写真の挿入場所を修正した
- **blowfish/content/posts/2024/20240617_gunma/index.md**: 写真の挿入場所を修正した
- **blowfish/content/posts/2024/20240617_gunma/index.md**: 文章を校正した
- **blowfish/content/posts/2024/20240618_tsuyama/index.md**: 文章を校正した
- **blowfish/content/posts/2024/20240618_tsuyama/index.md**: 記事がうまく表示されなくなったのを修正
- **blowfish/content/posts/2024/20240725_kagawa/IMG_20240725_featured.jpg**: カバー画像のファイル名を変更した

### Authors

- **blowfish/data/authors/ct-gunma.json**: 群馬高専を追加した

### Configuration

- **blowfish/config/_default/hugo.toml**: paginateをpagination.pageSizeに置き換えた
- **blowfish/config/_default/hugo.toml**: timeoutを設定: 50s
- **blowfish/config/_default/params.toml**: 著者の表示位置を変更した

### Themes

- **blowfish/themes/blowfish**: Blowfishを更新した: v2.75.0 -> v2.77.1

## 1.0.9 (2024-04-30)

### Fix

参加している機関名のauthorsファイルを追加した

- **blowfish/data/authors/ct-ibaraki.json**: 茨城高専を追加した
- **blowfish/data/authors/ct-kagawa.json**: 香川高専を追加した
- **blowfish/data/authors/ct-kure.json**: 呉高専を追加した
- **blowfish/data/authors/ct-nagano.json**: 長野高専を追加した
- **blowfish/data/authors/ct-nagaoka.json**: 長岡高専を追加した
- **blowfish/data/authors/ct-oyama.json**: 小山高専を追加した
- **blowfish/data/authors/ct-toyota.json**: 豊田高専を追加した
- **blowfish/data/authors/ct-tsuyama.json**: 津山高専を追加した
- **blowfish/data/authors/kek.json**: KEKを追加した
- **blowfish/data/authors/kyoto.json**: 京都大学を追加した
- **blowfish/data/authors/kyushu.json**: 九州大学を追加した
- **blowfish/data/authors/nagaoka.json**: 長岡技科大を追加した
- **blowfish/data/authors/nagoya.json**: 名古屋大学を追加した
- **blowfish/data/authors/riken.json**: 理研を追加した
- **blowfish/data/authors/sokendai.json**: 総研大を追加した
- **blowfish/data/authors/utsunomiya.json**: 宇都宮大学を追加した

## 1.0.8 (2024-04-09)

### Fix

- **blowfish/themes/blowfish**: テーマを更新した

## 1.0.7 (2024-04-09)

### Fix

- **.mise.toml**: Hugoのバージョン指定を追加した

## 1.0.6 (2024-03-08)

- パンフレットPDFのリンク間違いを修正した


### Fix

- **blowfish/content/_index.ja.md**: パンフレットPDFへのリンクのパスを修正した

## 1.0.5 (2024-03-07)

- テーマを更新した

### Fix

- **blowfish/themes/blowfish**: Blowfish v2.60.0 に更新した

## 1.0.4 (2024-03-07)

- 公開済みのURL（DocsyテーマのURL）からのリダイレクトを追加した

### Fix

- **blowfish/content/posts/2024/**: エイリアスを追加した

## 1.0.3 (2024-03-07)

- デプロイ用のスクリプトを修正した


### Fix

- **blowfish/config/_default/languages.en.toml**: 英語のサイト名を修正した
- **blowfish/config/_default/languages.ja.toml**: 日本語のサイト名を修正した
- **deploy.sh**: www2へデプロイするスクリプトを修正した

## 1.0.2 (2024-03-07)

- Docsyをサブモジュールから削除した

### Derecated

- **themes/docsy**: サブモジュールを削除した

## 1.0.1 (2024-03-07)

- Blowfishの設定をデフォルト値にした


### Fix

- **.gitlab-ci.yml**: HUGO_ENVをデフォルト値にした
- **.gitlab-ci.yml**: デプロイのルールを元に戻した
- **blowfish/config/_default/hugo.toml**: 一時的に削除した
- **blowfish/config/_default/hugo.toml**: 新しくデフォルト値を設定した
- **blowfish/config/blowfish/hugo.toml**: デフォルト値を移動した

## 1.0.0 (2024-03-07)

### BREAKING CHANGE

- テーマをBlowfishに変更した

### Feat

- **themes/blowfish**: Blowfishテーマをサブモジュールで追加した
- **data/authors**: Multi-authorの設定をしてみた

### Fix

- **assets/authors**: author用の仮画像を追加した
- **blowfish/archetypes/default.md**: 使いそうなフロントマターを追記した
- **blowfish/assets/**: 画像を移動した
- **blowfish/assets/featured-background.jpg**: デフォルトの背景画像を追加した
- **blowfish/assets/icons/**: FA6のSVGアイコンを追加した
- **blowfish/assets/logo/axltn.png**: 透過ロゴを追加した
- **blowfish/config/_default/hugo.toml**: デフォルト設定を追加した
- **blowfish/config/**: 設定ファイルを移動した
- **blowfish/config/blowfish/hugo.toml**: GA4タグを追加した
- **blowfish/config/blowfish/hugo.toml**: タクソノミーにauthorsを追加した
- **blowfish/config/blowfish/menus.ja.toml**: タグ一覧を追加した
- **blowfish/config/blowfish/menus.toml**: 日英メニューを追加した
- **blowfish/config/blowfish/params.toml**: article.heroStyleを変更した
- **blowfish/config/blowfish/params.toml**: cardViewを変更した
- **blowfish/config/blowfish/params.toml**: showMoreLinkを修正した
- **blowfish/config/blowfish/params.toml**: taxonomyのパラメータを明記した
- **blowfish/config/blowfish/params.toml**: デフォルトの background/featured 画像を変更した
- **blowfish/config/blowfish/params.toml**: トップページのcardViewScreenWidthの設定を変更した
- **blowfish/config/blowfish/params.toml**: やっぱり posts にする
- **blowfish/config/blowfish/params.toml**: 最新記事は6件にした
- **blowfish/config/www2/hugo.toml**: デプロイ用の設定ファイルを追加した
- **blowfish/content/_index.ja.md**: トップページを修正した
- **blowfish/content/_index.ja.md**: ファイル名を変更した
- **blowfish/content/_index.md**: トップページの文章を見直した
- **blowfish/content/_index.md**: トップページを移動した
- **blowfish/content/about/_index.ja.md**: aboutページを修正中
- **blowfish/content/about/_index.ja.md**: content/ja/about/_index.html の内容を移動した
- **blowfish/content/about/_index.ja.md**: ロゴを挿入した
- **blowfish/content/about/**: Aboutを追加した
- **blowfish/content/blog/20200401_axltn/index.ja.md**: ファイルの場所を検討中
- **blowfish/content/blog/news/20200401/20200401_axltn.md**: ファイルの移行テスト
- **blowfish/content/members/index.ja.md**: メンバー一覧を更新した
- **blowfish/content/members/index.ja.md**: メンバー一覧を追加した
- **blowfish/content/posts/_index.ja.md**: 追加した
- **blowfish/content/posts/20200401_axltn/index.ja.md**: エイリアスを追加した
- **blowfish/content/posts/20200920_axltn_mft2020/index.md**: MFT2020の予告記事を移動した
- **blowfish/content/posts/20201005_axltn_mft2020/index.md**: MFT2020参加報告
- **blowfish/content/posts/20201005_axltn_mft2020/index.md**: MFT2020参加報告を移行した
- **blowfish/content/posts/20201202_ibaraki/index.md**: タイトルを修正した
- **blowfish/content/posts/20201202_ibaraki/index.md**: 茨城高専のリモートセミナーを追加した
- **blowfish/content/posts/20201207_ibaraki/index.md**: タグを追加した
- **blowfish/content/posts/2022/20220117_oyama/index.md**: キャプションを修正した
- **blowfish/content/posts/20220117_oyama/index.md**: 修正した
- **blowfish/content/posts/2024/20240115_kagawa/index.md**: 香川高専の記事を移行した
- **blowfish/content/posts/2024/20240117_tsuyama/index.md**: 津山高専の記事を移行した
- **blowfish/content/posts/2024/20240220_oyama/index.md**: 高専交流会の記事を移行した
- **blowfish/content/posts/2024/20240301_kagawa/index.md**: 香川高専の記事を移行した
- **blowfish/content/videos/cyclotron/_index.md**: 佐藤さんの動画を並べた
- **blowfish/content/videos/cyclotron/**: 動画紹介のサンプルを追加した
- **blowfish/content/videos/cyclotron/chapter1/index.ja.md**: ファイル名を変更した
- **blowfish/data/authors/**: Multi-Authors用のファイルを移動した
- **blowfish/data/authors/hattori.json**: 著者を追加した
- **blowfish/hugo.toml**: もういちどBlowfishテーマのサイトを作り直すことにした
- **blowfish/layouts/shortcodes/imgproc.html**: 置き換え先のコードを表示するようにした
- **blowfish/layouts/shortcodes/imgproc.html**: 記事を移行するためのダミーショートコードを移動した
- **blowfish/static/booklet.pdf**: パンフレットを移動した
- **blowfish/static/favicon.ico**: faviconを追加した
- **CONTRIBUTING.md**: 貢献する方法を追加した
- **LICENSE**: プロジェクトを開始した年に修正した
- **.gitlab-ci.yml**: CIの期限（180 days）を追加した

### Deprecated

- **.gitlab-ci-docsy.yml**: Docsy用のCIのバックアップを作成した
- **config/blowfish/hugo.toml**: グローバルなパラメータを設定中
- **config/blowfish/hugo.toml**: テーマをblowfishだけにした
- **config/blowfish/hugo.toml**: パーマリンクを追加した
- **config/blowfish/languages.en.toml**: 移行の準備のためにenサイトを活用する
- **config/blowfish/languages.fr.toml**: テスト用にフランス語サイトを作成した
- **config/blowfish/languages.fr.toml**: フランス語設定は必要なかった
- **config/blowfish/languages.ja.toml**: 日本語のパラメータを設定中
- **config/blowfish/menus.en.toml**: メニュー名の重複を一時的に回避した
- **config/blowfish/menus.en.toml**: メニュー用のパラメータを設定した
- **config/blowfish/params.toml**: mainSectionsにblogも追加した
- **config/blowfish/params.toml**: mainSectionsをpostだけに戻した
- **config/blowfish/params.toml**: mainSectionsを追加した
- **config/blowfish/params.toml**: タグ一覧のパラメータを設定した
- **config/blowfish/params.toml**: トップページのパラメータを設定した
- **config/blowfish/params.toml**: 一覧ページのパラメータを設定した
- **config/blowfish/params.toml**: 記事のパラメータを設定した
- **content/en/_index.html**: コンテンツを削除した
- **content/en/_index.md**: ファイル名を変更した
- **content/en/about/_index.md**: enディレクトリで動作テスト中
- **content/en/featured-background.jpg**: 不要な画像を削除した
- **content/en/posts/20201005/index.md**: サンプルを追加した
- **content/en/posts/20201005/index.md**: タグとカテゴリーを追加した
- **content/en/search.md**: 不要なファイルを削除した
- **content/no/**: no語を削除した
- **layouts/_default/**: Docsyに合わせた独自テンプレートを削除した
- **layouts/shortcodes/blocks/**: Docsyに依存しているショートコードのダミーを作成した

### Migrated

- 2020年の活動を整理した
- 2021-01-13: 記事を移行した
- 2021-02-26: 記事を追加した
- 2021-03-03: 記事を移行した
- 2021-03-04: 記事を移行した
- 2021-03-12 : 記事を移行した
- 2021-03-26 : 記事を移行した
- 2021-04-22: 記事を移行した
- 2021-06-15: 記事を移行した
- 2021-08-18: 記事を移行した
- 2021-10-26: 記事を移行した
- 2021-11-10: 記事を移行した
- 2021-12-15: 記事を移行した
- 2021-12-20: 記事を移行した
- 2021-12-21: 記事を移行した
- 2021年の記事を整理した
- 2022-02-22: 記事を移行した
- 2022-03-11: 記事を移行した
- 2022-03-12: 記事を移行した
- 2022-04-12: 記事を移行した
- 2022年の記事を移行した
- 2023-01-17: 加筆修正した
- 2023年の記事を移動した
- 2023年の記事を移行した
- HEIF画像は読み込めないので、JPGに変換した
- カバー画像を設定した（ファイル名の末尾に_featuredを追加した
- ファイルを移動した
- メンバーをメニューに追加した
- リダイレクトを追加した
- 一時的に追加したショートコードを削除した
- 不要なファイルを削除した
- 小山高専・レーザー説明会を移動した
- 茨城高専での活動記録を追加（2021年の記事）
- 茨城高専説明会（第１回）の記事を移行した


## 0.1.1 (2024-03-06)

### Fix

- **config/_default/config.toml**: デフォルト言語は日本語
- **config/_default/config.toml**: algoriaの設定をdocsyの仕様変更に従って修正した
- **config/_default/languages.toml**: 言語設定をHugoの仕様変更に従って修正した
- **config/docsy/config.toml**: Docsyの設定を移動した
- **config/docsy/markup.toml**: Docsyのmarkup設定を分割した
- **config/docsy/params.toml**: Docsyテーマの設定を分割した
- **config/www2/config.toml**: www2用の設定を修正した
- **content/ja/blog/news/20231013/20231013T180703.png**: 画像のファイル名を変更した
- **content/ja/blog/news/20231013/index.md**: linkTitleを修正した
- **content/ja/blog/news/20231013/index.md**: 参加人数を修正した
- **content/ja/blog/news/20231106/index.md**: linkTitleを修正した
- **content/ja/blog/news/20231106/index.md**: 文章を微修正した
- **content/ja/blog/news/20231120/index.md**: 改行を追加した
- **content/ja/blog/news/20240115/index.md**: YAML delimiterを修正した
- **content/ja/blog/news/20240115/index.md**: 本文を修正した
- **content/ja/blog/news/20240115/index.md**: 関連リンクのセクションを削除した
- **content/ja/blog/news/20240117/index.md**: YAML delimiterを修正した
- **content/ja/blog/news/20240117/index.md**: 本文を修正した
- **content/ja/blog/news/20240220/index.md**: 交流会の文章を修正した
- **content/ja/blog/news/20240301/index.md**: 記事のタイトルを修正した
- **content/ja/blog/news/20240301/index.md**: 重複していた内容を削除した
- **package.json**: buildコマンドを修正した
- **themes/docsy**: Updated to Docsy 0.7.2

## 0.1.0 (2023-12-01)


### Published

- **content/ja/blog/news/20231013/index.md**: レーザー技術講習会 by ネオアーク株式会社
- **content/ja/blog/news/20231106/index.md**: 8th STI Gigaku 2023のポスター発表 by 小山高専
- **content/ja/blog/news/20231120/index.md**: 加速器に関する講演会 at 香川高専

### Fixed

- **config/_default/config.toml**: algoriaの設定をdocsyの仕様変更にしたがって修正した
- **config/_default/languages.toml**: 言語設定をHugoの仕様変更にしたがって修正した
- **themes/docsy**: Updated to Docsy 0.7.2

## 0.1.0 (2023-10-15)

### Added

- **content/ja/videos/_index.md**: 動画リスト（仮）を作成した
- **videos**: 動画をまとめるセクションを追加した

### Fixed

- **config/_default/config.toml**: GA4タグに置き換えた
- **config/www2/config.toml**: enableGitInfo を falseにした
- **config/www2/config.toml**: GA4タグを追加した
- **content/ja/videos/_index.md**: FAアイコンを追加した
- **content/ja/videos/_index.md**: PDF資料の共有リンクを追加した
- **content/ja/videos/_index.md**: YouTubeのアイコンを追加した
- **content/ja/videos/_index.md**: 準備中のタグを追加した

### Published

- **content/ja/blog/news/20230219/index.md**: 文章を微修正しました
- **content/ja/blog/news/20230407/index.md**: 文科大臣表彰の記事を公開した
- **content/ja/blog/news/20230611/index.md**: 小山高専の記事を公開した
- **content/ja/blog/news/20230612/index.md**: 津山高専説明会の記事を公開した
- **content/ja/blog/news/20230613/index.md**: 豊田高専の記事を公開した
- **content/ja/blog/news/20230620/index.md**: 豊田高専と小山高専の交流会の記事を公開した

### Build

- **deploy.sh**: npm run www2に置き換えた
- **deploy.sh**: public は削除しないことにした
- **package.json**: gitlab用スクリプトを削除した（buildと同じだから）
- **package.json**: npm run www2を追加した

### Package Updates

- **themes/docsy**: v0.7.1に更新した
- **package.json**: Hugo 0.119.0 に更新した
- **package.json**: npmパッケージを更新した

## 0.0.4 (2023-01-23)

### Fix

- **content/ja/blog/news/20220904/index.md**: 写真を削除した

## 0.0.3 (2023-01-23)

### Fix

- **content/ja/blog/news/20220904/index.md**: 記事を公開した
- **content/ja/blog/news/20220912/index.md**: 記事を公開した

## 0.0.2 (2023-01-23)

### Fix

- **package.json**: prepareスクリプトを追加した
- **content/ja/blog/news/20230117/index.md**: 記事を公開した
- **content/ja/blog/news/20230117/index.md**: 括弧を全角に変更した
- **content/ja/blog/news/20220904/index.md**: MFT記事を修正した
- **content/ja/blog/news/20220904/index.md**: 記事のファイル名が重複しているのでひとまず削除した
- **content/ja/blog/news/20220912/index.md**: 役物とリンクを修正した
- **content/ja/blog/news/20220912/index.md**: 下書きモードに変更した
- **content/ja/blog/news/20220904/index.md**: MFT2022の記事を修正した
- **content/ja/blog/news/20220904/index.md**: 下書きにした
- 2022-07-12の記事を公開
- **content/ja/blog/news/20220712/DSCN3116_featured.JPG**: 自動でサムネイルに選択されるようにファイル名を変更した
- **content/ja/blog/news/20220712/index.md**: 既に -> すでに。テキスト校正くんにしたがって、ひらがなにした
- **content/ja/blog/news/20220712/index.md**: 下書きモードに変更した
- **content/ja/blog/news/20220712/index.md**: 2段落目を修正。一文が長かったので短くしてみた。
- **content/ja/blog/news/20220712/index.md**: 3段落目を修正しました
- **content/ja/blog/news/20220712/index.md**: 交流会 -> オンライン交流会に強調
- **content/ja/blog/news/20220628/index.md**: publish article
- **content/ja/blog/news/20220628/index.md**: 下書きモードにした
- **content/ja/blog/news/20220628/IMG_20220628_175916_featured.jpg**: カバー画像となるようにファイル名を変更した
- **LICENSE**: LICENSEを一旦削除した
- **themes/docsy**: サブモジュール（themes/docsy）を更新した
- **content/ja/blog/news/20220517/index.md**: 本文を一部修正した
- **content/ja/blog/news/20220517/index.md**: 本文を一部修正した
- **content/ja/blog/news/20220517/index.md**: 真空度の単位（Pa）を追加した
- **content/ja/blog/news/20220517/IMG_20220517_174617_featured.jpg**: 画像のファイル名を修正した
- **content/ja/blog/news/20220517/index.md**: 冪乗をsupタグで表現した
- **content/ja/blog/news/20220517/index.md**: shortcodesのタイポを修正した
- **content/ja/blog/news/20220426/index.md**: 画像のファイル名を変更した（featuredを追加した）
- **content/ja/blog/news/20220426/index.md**: 活動 -> 研究 のように文章を修正した
- **content/ja/blog/news/20220412/index.md**: 文章を少し修正した
- **content/ja/blog/news/20220312/index.md**: 文章を修正した
- **content/ja/blog/news/20220312/slides_featured.jpeg**: カバー画像を追加した
- **content/ja/blog/news/20220311/index.md**: 記事のタイトルとアイキャッチ画像を修正した
- **community/_index.md**: メンバーリストを更新した
- **2022-02-22**: 小山高専がJrセッションで発表する記事を公開した
- **CONTRIBUTING.md**: 内容を修正した
- **deploy.sh**: デプロイ用のスクリプトを修正した
- **config.toml**: デフォルトのサイト設定を見直した
- **config**: サイト設定（のファイルパス）を見直した
- Jrセッションの記事を微修正した
- **.gitignore**: ignore hugo related lock files
- **.gitignore**: ignore macOS related files
- **static/booklet.pdf**: パンフレット（PDF）を追加した
- **content/ja/blog/news/20220117/IMG_20220117_171717_featured.jpg**: 記事一覧のサムネイル画像を追加
- **content/ja/blog/news/20220117/index.md**: タグを修正：長岡高専->小山高専
- **i18n/ja.toml**: added missing translation
- **content/ja/blog/news/*.md**: 記事にタグを追加した
- **config/_default/config.toml**: タグを使えるようにデフォルト設定を変更した
- **package.json**: fixed npm run build
- **config/gitlab/config.toml**: GitLab CI用の設定ファイルを追加した
- **config.toml**: fixed _default and production config values
- **config/production/config.toml**: production用の設定ファイルを追加した
- **config/staging/config.toml**: ステージング環境用の設定を追加した
- **config/_default/config.toml**: コピーライトの表示を修正した
- **content/ja/about/_index.html**: fixed typo
- **config/_default/config.toml**: フッターのアイコンを整理した
- **config/_default/languages.toml**: 言語設定用のファイルを作成した
- **config/_default/config.toml**: 設定ファイルの配置を変更した
- **layouts/_default/list.rss.xml**: RSSのdescriptionの表示内容を変更した
- **layouts/_default/list.rss.xml**: copied template from themes/docsy
- **config.toml**: hasCJKLanguage = true の設定を追加した
- **content/ja/blog/news/20211221/index.md**: 長岡高専の記事を公開
- **content/ja/blog/news/20211220/index.md**: 長野高専の記事を公開
- **layouts/docs/baseof.html**: 右サイドバーから page-meta-links を削除した
- **layouts/blog/baseof.html**: 右サイドバーから page-meta-links を削除した
- **i18n/ja.toml**: 日本語のi18nファイルを追加した
- **/20211215**: 不要なディレクトリを削除した
- **content/ja/blog/news/20211215/index.md**: 記事の内容が空になってしまっていたので、過去の履歴から内容をコピペした
- **content/ja/blog/news/20211215/.gitkeep**: removed .gitkeep
- **content/ja/blog/news/20211221/index.md**: 下書きに変更した（draft=true）
- **content/ja/blog/news/20211221/index.md**: 下書きに変更した（draft=true）
- **content/ja/blog/news/20211221/index.md**: タイトルを修正。「初めて」を追加した。写真2のキャプションを修正した。
- **content/ja/blog/news/20211220/index.md**: タイトルを修正。「初めて」を追加した。
- **20211215/index.md**: Nb3Snの修正し忘れを発見した
- **20211215/index.md**: 文章を微修正しました
- **content/ja/blog/news/20211215/index.md**: Nb3Snの3を下付きに変更した
- **rm**: removed directory : /20211215
- **content/ja/blog/news/20210818/index.md**: published article
- **content/ja/blog/news/20211026/index.md**: published article
- **content/ja/blog/news/20211110/index.md**: 記事を公開モードに変更した
- **content/ja/blog/news/20211110/index.md**: linkTitle を微修正した
- **content/ja/blog/news/20211110/index.md**: 誤字を修正
- **content/ja/docs/Examples/**: ディレクトリを削除した
- **content/ja/docs/Tutorials/**: ディレクトリを削除した
- **content/ja/docs/Contribution...**: ディレクトリを削除した
- **content/ja/docs/Concepts/**: ディレクトリを削除した
- **content/ja/docs/Overview/_index.md**: 加速器の説明ページを修正
- **content/ja/docs/_index.md**: サンプル文を削除した
- **20210818/index.md**: add article : 小山高専で本格的に活動を開始しました
- **20210615/index.md**: fix article
- **20210615/index.md**: add new article : 小山高専で説明会を開催しました
- **2021-04-22**: published article
- **2021-03-26**: published article
- **2021-04-22**: fixed typo
- **2021-04-22**: removed 写真
- **draft**: 2020-03-26 : 作業風景 : 加速器の試運転
- **draft**: 2021-03-12 : 作業風景 : 銅ガスケットの取り付け
- **article**: draft : 2020-04-22 2021年度活動開始！
- **article**: draft : 2020-03-26 : 作業風景
- **article**: draft : 2021-03-12 : 作業風景（仮タイトル）
- **article**: draft : 2020-03-04 : 茨城高専（第3回）セミナー
- **article**: published : 2021-03-03 カソードを製作
- **article**: published : 2020-12-02 茨城高専でリモートセミナーを開催
- **article**: draft : 2020-12-02 茨城高専でリモートセミナーを開催しました
- **article**: draft : 2021-03-03 カソードを製作しました
- **article**: draft : 2021-02-26 サイクロトロン加速器の組み立て開始
- **article**: published : 2021-01-15 加速器製作のための物品手配
- **blog**: add article: 2020-01-15
- **blog**: add article : 2020-01-13
- **blog**: add article : 2020-12-07

### Refactor

- **content/ja/blog/news/20210226/index.md**: 日本語校正ツールに従って修正した
- **mv**: moved files under Date directory
