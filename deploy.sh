### www2にアップロードするスクリプト

# echo "STEP 0 : Remove exsiting public directory"
# rm -rf public/

echo "STEP 1 : Build hugo"
cd blowfish
hugo -e www2 || exit 1

echo "STEP 2 : Add g+rw priviledges to public"
chmod -R g+rw ./public/

echo "STEP 3 : Upload to www2"
#rsync -auvz --delete public/ kek2:~/axltn/
rsync -auvz ./public/ kek2:~/axltn/
